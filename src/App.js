import NewExpense from "./components/NewExpense/NewExpense";
import Expenses from "./components/Expenses/Expenses";

const  App = () => {

  const expenses = [
    { _id: 'e1', _title: 'Rent property', _amount: 594.12, _date: new Date(2020, 7, 14) },
    { _id: 'e2', _title: 'New TV', _amount: 799.49, _date: new Date(2021, 2, 12) },
    { _id: 'e3', _title: 'Car Insurance', _amount: 294.67, _date: new Date(2021, 1, 8) },
    { _id: 'e4', _title: 'New Desk', _amount: 99.99, _date: new Date(2021, 9, 28) },
  ];

  // return React.createElement(
  //   'div',
  //   {},
  //   React.createElement('h2', {}, "Let's get started!"),
  //   React.createElement(Expenses, { items: expenses })
  // );
 
  return (
    <div>
      <NewExpense />
      <Expenses items={expenses} />
    </div>
  );
}

export default App;


