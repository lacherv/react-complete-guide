import './card.css';

function Card(props) {
    const classes = 'card ' + props.className; // put space after the 'card '
    
    return (
        <div className={classes}>
            {props.children}
        </div>
    )
}

export default Card;


