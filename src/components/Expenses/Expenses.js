import Card from '../UI/Card';
import ExpenseItem from './ExpenseItem';
import './expenses.css';


function Expenses(props) {

    return (
        <Card className='expenses'>
            <ExpenseItem 
                title={props.items[0]._title}
                amount={props.items[0]._amount}
                date={props.items[0]._date}
            />
            <ExpenseItem
                title={props.items[1]._title}
                amount={props.items[1]._amount}
                date={props.items[1]._date}
            />
            <ExpenseItem
                title={props.items[2]._title}
                amount={props.items[2]._amount}
                date={props.items[2]._date}
            />
            <ExpenseItem
                title={props.items[3]._title}
                amount={props.items[3]._amount}
                date={props.items[3]._date}
            />
        </Card>
    )
}

export default Expenses;
