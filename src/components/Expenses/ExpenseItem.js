// import the CSS file
import React, { useState } from 'react';

import './expense-item.css';
// import components
import ExpenseDate from './ExpenseDate';
import Card from '../UI/Card';


const  ExpenseItem = (props) => {
    
    const [title, setTilte] = useState(props.title);
    console.log('ExpenseItem evaluated by react');

    const clickHandler = () => {
        setTilte('Updated')
        console.log(title);
    };

    return (
        <Card className="expense-item">
            <ExpenseDate date={props.date} />
            <div className="expense-item__description">
                <h2>{title}</h2>
                <div className="expense-item__price">R{props.amount}</div>
            </div>
            <button onClick={clickHandler}>Change Title</button>
        </Card>
    );
}

export default ExpenseItem;